﻿<#
.SYNOPSIS
    Injects Package into Windows 10 WIM
.DESCRIPTION
    Uses the Add-WindowsPackage DISM module command to inject .cab or .msu files 
    into an offline Image. You must have the DISM Add-in module loaded.
.PARAMETER Cache
    Location that you plan to expand the .Wim file to.
.PARAMETER Package
    Full path to the package you wish to add.
.PARAMETER Wim
    Full path to the windows OS wim that you wish to have the package injected into.
.PARAMETER NoClose
    Don't Unmount the Wim once done
.PARAMETER NoOpen
    Don't Mount the Wim at launch
.EXAMPLE
    .\Inject-Package.ps1 -Cache C:\WF\IMG -Package C:\WF\Target.cab -Wim C:\WF\install.wim
    Load Wim, inject package, and close WIM.
.EXAMPLE
    .\Inject-Package.ps1 -Cache C:\WF\IMG -Package C:\WF\Target.cab -NoOpen -NoClose
    Inject Package only.
.INPUTS
    No Pipes accepted.
.OUTPUTS
    No outputs sent to the pipe.
.NOTES
    Version: 1705
    Created By: Devin Hunter

.LINK
    https://bitbucket.org/Doltknuckle/powershell-toolkit
#>
PARAM(
    [STRING]$Cache = "C:\WF\IMG", #Path to cache folder
    [STRING]$Package = "C:\WF\microsoft-windows-netfx3-ondemand-package.cab", #Path to Package
	[STRING]$Wim = "C:\WF\install.wim", #Path to WIM
    [SWITCH]$NoClose, #Don't Close Wim
    [SWITCH]$NoOpen #Don't Open Wim
)
cls
################################################################################
# Test-Environment
# ****************************************
# Make sure that the locations are correct
# 
# -IN: Path to image cache
# -IN: Path to package to inject
# -IN: Path to Wim file
# -THROW: DISM Module not loaded
# -THROW: Files Not Found
# -THROW: User input on Cache folder clear -NEQ Y
# -THROW: User input on Cache folder create -NEQ Y
# -OUT: None
################################################################################
function Test-Environment {
	PARAM(
        [STRING]$ImagePath="",
		[STRING]$PackagePath="",
        [STRING]$WimFile=""
	)
    #Check for Wim storage location and create if needed.
	try{
        #Check for DISM Module
        if(!(Get-Module -Name Dism)){
            #DISM Missing
            THROW "DISM Module not loaded from ADK"
        }
        #Check Archive Folder path
        if(!(Test-Path $PackagePath)){
            THROW "Package folder/file does not exist"
        }

		#Check Image storage location
		if(Test-Path $ImagePath){
            #Image folder found, Check for mounted image
            $bin = Get-WindowsImage -Mounted | ?{$_.ImagePath -eq $Wim}
            if(!($bin)){
                #No mounted image, Clean any files.
                $ChildItems = Get-ChildItem -Path $ImagePath
			    if((Get-ChildItem -Path $ImagePath)){
                    Write-Host $ImagePath
                    $Prompt = Read-Host "Files found in Image Cache folder, Delete? (Y/n)"
                    if(($Prompt -ieq "Y") -or ($Prompt -eq "")){
                        Get-ChildItem -Path $ImagePath -Recurse | ForEach-Object{Remove-Item $_.FullName -Recurse }
                    } else {
                        THROW "Cache folder not empty."
                    }
                #End ChildItem check.
                }
            #End Image Mount check.
            }
		} else {
            #Image folder not found, create one
            $Prompt = Read-Host "Folder not found, Create? (Y/n)"
            if(($Prompt -ieq "Y") -or ($Prompt -eq "")){
			    New-Item $ImagePath -ItemType direc
            } else {
                THROW "Cache folder not created."
            }
		}

		#Test WinFile
		if(!(Test-Path $WimFile) -and !($NoOpen)){
			THROW "Wim File does not exist."
		} else {
            #Turn off read-only
            Set-ItemProperty $WimFile -Name IsReadOnly -Value $False
        }
    #END Try Block
	} Catch {
        Write-Warning "[CATCH] Errors found during Test-Environment, check your inputs. Script Halted."
        Write-Warning $_.Exception.Message
        Write-Host "ImagePath="$ImagePath
		Write-Host "PackagePath="$PackagePath
        Write-Host "WimFile"=$WimFile
    	BREAK
	}
}

################################################################################
# Open-Wim
# ****************************************
# Creates the workarea and mounts the Wim to that location.
# 
# -IN: Path to image cache
# -IN: Path to Wim file
# -OUT: None
################################################################################
function Open-Wim {
	PARAM(
		[STRING]$WimPath,
		[STRING]$MountPath
	)
	Try{
		Mount-WindowsImage -Path $MountPath -ImagePath $WimPath -Index 1
	}
	Catch{
		Write-Warning "[CATCH] Errors during Open-Wim. Script Halted."
        Write-Warning $_.Exception.Message
        $Path
        $ImagePath
    	BREAK
	}
}

################################################################################
# Close-Wim
# ****************************************
# Closes Wim and saves changes.
# 
# -IN: Path to image cache
# -OUT: None
################################################################################
function Close-Wim {
	PARAM(
		[STRING]$MountPath
	)
	Try{
		DisMount-WindowsImage -Path $MountPath -Save -CheckIntegrity
	}
	Catch{
		Write-Warning "[CATCH] Errors during Close-Wim. Script Halted."
        Write-Warning $_.Exception.Message
    	BREAK
	}
}
################################################################################
# Inject-Target
# ****************************************
# Use DISM to inject package
# 
# -IN: Path to image cache
# -IN: Path to package to inject
# -THROW: Package version does not match Wim version.
# -OUT: None
################################################################################
function Inject-Target{
    PARAM(
		[STRING]$ImagePath,
        [STRING]$PackagePath
	)
    Try{
		#Get Image Version
        $mountedImage = Get-WindowsImage -Mounted | ?{$_.Path -eq $ImagePath}
        $targetImage = Get-WindowsImage -ImagePath $mountedImage.ImagePath -Index 1

        #Get Package Version
        $targetPackage = Get-WindowsPackage -Online -PackagePath $PackagePath

        #Test Package version
        $matchResult = $targetPackage.PackageName | Select-String -Pattern $targetImage.Version -SimpleMatch
        if($matchResult){
            #Match found, Inject
            Add-WindowsPackage -PackagePath $PackagePath -Path $ImagePath
        } else {
            #Match not found
            THROW "Injection Target does not match OS version:"+$targetImage.Version
        }

	}
	Catch{
		Write-Warning "[CATCH] Errors during Inject-Target. Script Halted."
        Write-Warning $_.Exception.Message
    	BREAK
	}
}

##########
# Procedure
##########
Test-Environment-ImagePath -ImagePath $Cache -PackagePath $Package -WimFile $Wim
if(!($NoOpen)){Open-Wim -WimPath $Wim -MountPath $Cache}
Inject-Target -ImagePath $Cache -PackagePath $Package
if(!($NoClose)){Close-Wim -MountPath $Cache}