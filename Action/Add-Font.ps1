﻿<#
.SYNOPSIS
    Install Font to local system
.DESCRIPTION
    Uses the Font COM object method CopyHere() to add fonts to the system. Can be a 
    folder or a single font.
.PARAMETER Path
    Location of the fonts to add.
.PARAMETER Verbose
    Show custom status messages
.EXAMPLE
    .\Add-Font.ps1 -Path C:\WF\Fonts\
    Adds all fonts in a folder.
.EXAMPLE
    .\Add-Font.ps1 -Path C:\WF\Fonts\TestFont.ttf
    Adds only the font specified.
.INPUTS
    No Pipes accepted.
.OUTPUTS
    No outputs sent to the pipe.
.NOTES
    Version: 1705
    Created by: Devin Hunter

    Script has only been tested with TrueType and OpenType Fonts.
    ==USE AT YOUR OWN RISK!==
.LINK
    https://bitbucket.org/Doltknuckle/powershell-toolkit
#>
PARAM(
    [STRING]$Path = "C:\WF\Fonts\", #Path to font folder
    [SWITCH]$Verbose
)
cls
##########
#Prestart logic
##########
if($Verbose){
    $VerbosePreference = "Continue"
    } else {
    $VerbosePreference = "SilentlyContinue"
    }

################################################################################
# Get-Font
# ****************************************
# Load font files from the location specified
# 
# -IN: Path to folder
# -THROW: Path Not Found
# -OUT: Pipeline object
################################################################################
function Get-Font{
    PARAM(
        [STRING] $Path #Path to Font folder/file
    )
    Try{
        If(!(Test-Path $Path)){
            THROW "Path parameter target not found"
        } else {
            $target = Get-Item $Path
            #Check for folder
            if($target.PSIsContainer){
                #Folder Found
                Write-Verbose "Target is a folder"
                $items = Get-ChildItem $target -Recurse -Include *.ttf,*otf
                Write-Output $items
            } else {
                #File Found
                Write-Verbose "Target is a file"
                $item = Get-Item -Path $target
                Write-Output $item
            }
        #End Test-Path
        }
    }
    Catch{
		Write-Warning "[CATCH] Errors during Get-Font. Script Halted."
        Write-Warning $_.Exception.Message
        Write-Host $Path
    	BREAK
	}

}
################################################################################
# Load-Font
# ****************************************
# Add Font to windows system
# 
# -IN: Font Object
# -OUT: Array of font names
################################################################################
function Load-Font {
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$True,
		ValueFromPipeline=$True)]
		[OBJECT]$font
	)

	BEGIN {
        #Build list of installed fonts
        Write-Verbose "Build Registry list of fonts"
        $registryList = Get-Item "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Fonts" | Select-Object -ExpandProperty Property
    }

	PROCESS {
        Try{
            If(!(Test-Path $font.FullName) -or $font.PSIsContainer){
                #File Missing or folder passed
                THROW "Unable to find font file specified"
            } else {
                #Build Font handler
                Write-Verbose "******"
                Write-Verbose "Target File Valid: $font"
                $shellApp = New-Object -ComObject shell.application
                $fontApp = $shellApp.NameSpace($font.Directory.FullName)
                $fileApp = $fontApp.ParseName($font.Name)

                #Get Font file details by searching through file details.
                for($i=0;$i -le 287;$i++){
                    #Get Font Name
                    if($fontApp.GetDetailsOf($null,$i) -eq "Title"){
                        Write-Verbose "Title found: $i"
                        $fileTitle = $fontApp.GetDetailsOf($fileApp,$i)
                        Write-Verbose $fileTitle
                    }

                    #Get Font Type
                    if($fontApp.GetDetailsOf($null,$i) -eq "Type"){
                        Write-Verbose "Type found: $i"
                        $fileType = $fontApp.GetDetailsOf($fileApp,$i)
                        
                        #Condense to first word only Ex: TrueType font file => TrueType
                        $binSpace = $fileType.IndexOf(" ")
                        if($binSpace -gt 0){$fileType = $fileType.Substring(0,$fileType.IndexOf(" "))}

                        Write-Verbose $filetype
                    }
                    
                }
                $fontSerachString = $fileTitle+" ("+$fileType+")"
                #Note: OpenType fonts sometimes display as TrueType Fonts in registry.
                $fontSearchTrueType = $fileTitle+" (TrueType)"

                Write-Verbose "Search String: $fontSerachString"
                Write-Verbose "******"

                #Check for font
                if(!($registryList -contains $fontSerachString) -and !($registryList -contains $fontSearchTrueType)){
                    #Font file not loaded yet
                    Write-Host -NoNewLine "Loading: $fontSerachString"
                    $fontApp = $shellApp.NameSpace(0x14) #Target the System Font Folder
                    $fontApp.CopyHere($font.FullName) #Copy font into folder
                    Write-Host " [Done]" -ForegroundColor Green
                } else {
                    Write-Host "Skipping: $fontSerachString"
                }
            }
        } Catch {
            Write-Warning "[CATCH] Errors during Load-Font. Script Halted."
            Write-Warning $_.Exception.Message
            Write-Host $font
    	    BREAK
        }
    }
}

##########
# Procedure
##########
Get-Font $Path | Load-Font