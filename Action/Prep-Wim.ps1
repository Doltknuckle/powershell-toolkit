﻿<#
.SYNOPSIS
    Prepares Windows 10 Reference WIM for OS Deployment through SCCM.
.DESCRIPTION
    Process removes extra AppX Packages and overwrites the default start menu.
.PARAMETER Cache
    Local drive location to unpack wim to.
.PARAMETER Wim
    Location of target WIM file.
.PARAMETER NoClose
    Don't Unmount the Wim once done
.PARAMETER NoOpen
    Don't Mount the Wim at launch
.EXAMPLE
    .\Prep-Wim.ps1 -Path C:\Folder\IMG -Wim C:\Folder\install.wim 
    Open Wim, Run process, and close wim.
.EXAMPLE
    .\Prep-Wim.ps1 -Path C:\Folder\IMG -Wim C:\Folder\install.wim -NoOpen -NoClose 
    Prepare Wim that is already open.
.INPUTS
    No Pipes accepted.
.OUTPUTS
    No outputs sent to the pipe.
.NOTES
    Version: 1705
    Created By: Devin Hunter

.LINK
    https://bitbucket.org/Doltknuckle/powershell-toolkit
#>

PARAM(
	[string]$Cache = "C:\WF\IMG", #Path to cache file
	[string]$Wim = "C:\WF\install.wim", #Path to WIM
    [SWITCH]$NoClose,
    [SWITCH]$NoOpen
)
cls

################################################################################
# Test-Environment
# ****************************************
# Make sure that the locations are correct
# 
# -IN: Path to image cache
# -IN: Path to Wim file
# -THROW: DISM Module not loaded
# -THROW: Files Not Found
# -THROW: User input on Cache folder clear -NEQ Y
# -THROW: User input on Cache folder create -NEQ Y
# -OUT: None
################################################################################t
function Test-Environment {
	PARAM(
		[STRING]$ImagePath,
		[STRING]$WimFile
	)
	try{
		#Check Image storage location
		if(Test-Path $ImagePath){
            #Image folder found, Check for mounted image
            $bin = Get-WindowsImage -Mounted | ?{$_.ImagePath -eq $Wim}
            if(!($bin)){
                #No mounted image, Clean any files.
                $ChildItems = Get-ChildItem -Path $ImagePath
			    if((Get-ChildItem -Path $ImagePath)){
                    Write-Host $ImagePath
                    $Prompt = Read-Host "Files found in Image Cache folder, Delete? (Y/n)"
                    if(($Prompt -ieq "Y") -or ($Prompt -eq "")){
                        Get-ChildItem -Path $ImagePath -Recurse | ForEach-Object{Remove-Item $_.FullName -Recurse }
                    } else {
                        THROW "Cache folder not empty."
                    }
                #End ChildItem check.
                }
            #End Image Mount check.
            }
		} else {
            #Image folder not found, create one
            $Prompt = Read-Host "Folder not found, Create? (Y/n)"
            if(($Prompt -ieq "Y") -or ($Prompt -eq "")){
			    New-Item $ImagePath -ItemType direc
            } else {
                THROW "Cache folder not created."
            }
		}
		#Test WinFile
		if(!(Test-Path $WimFile) -and !($NoOpen)){
			THROW "Wim File does not exist."
		} else {
            #Turn off read-only
            Set-ItemProperty $WimFile -Name IsReadOnly -Value $False
        }
	}
	Catch {
        Write-Warning "[CATCH] Errors found during Test-Environment, check your inputs. Script Halted."
        Write-Warning $_.Exception.Message
        Write-Host "ImagePath="$ImagePath
        Write-Host "WimFile"=$WimFile
    	BREAK
	}
}

################################################################################
# Open-Wim
# ****************************************
# Creates the workarea and mounts the Wim to that location.
# 
# -IN: Path to image cache
# -IN: Path to Wim file
# -OUT: None
################################################################################
function Open-Wim {
	PARAM(
		[STRING]$WimPath,
		[STRING]$MountPath
	)
	Try{
		Mount-WindowsImage -Path $MountPath -ImagePath $WimPath -Index 1
	}
	Catch{
		Write-Warning "[CATCH] Errors during Open-Wim. Script Halted."
        Write-Warning $_.Exception.Message
    	BREAK
	}
}

################################################################################
# Close-Wim
# ****************************************
# Closes Wim and saves changes.
# 
# -IN: Path to image cache
# -OUT: None
################################################################################
function Close-Wim {
	PARAM(
		[STRING]$MountPath
	)
	Try{
		DisMount-WindowsImage -Path $MountPath -Save -CheckIntegrity
	}
	Catch{
		Write-Warning "[CATCH] Errors during Close-Wim. Script Halted."
        Write-Warning $_.Exception.Message
    	BREAK
	}
}

################################################################################
# Clean-AppXPackages
# ****************************************
# Removes all Provisioned packages execpt those on the safe list.
# 
# -IN: Path to image cache
# -THROW: Unable to find image cache.
# -OUT: None
################################################################################
function Clean-AppxPackage{
	PARAM(
		[STRING]$ImagePath
	)	
	$safeList = @(
	    "Microsoft.Appconnector",
	    "Microsoft.ConnectivityStore",
	    "Microsoft.WindowsCalculator",
	    "Microsoft.WindowsStore"
	)
	#Process Packages
	if(Test-Path $ImagePath){    
		$appList = Get-AppxProvisionedPackage -Path $ImagePath
		Foreach ($package in $appList){
		    Write-Output $package.DisplayName
		    if(!($package.DisplayName -in $safeList)){
		        Remove-AppxProvisionedPackage -Path $ImagePath -PackageName $package.PackageName
		    }
		}
	} else {
		Write-Warning "[THROW] Errors during Clean-AppxPackage. Script Halted."
		Throw "Unable to find Mount-Point"
		BREAK
	}
}

################################################################################
# Empty-StartMenu
# ****************************************
# Add new LayoutModification.xml to image
# 
# -IN: Path to image cache
# -OUT: None
################################################################################
function Empty-StartMenu{
	PARAM(
		[STRING]$ImagePath
	)

    try{
        #Define file location inside image.
	    $target = $ImagePath+"\Users\Default\AppData\Local\Microsoft\Windows\Shell"
	   
        #Setup file location cache on local system.
        $filePath = $env:TEMP+"\LayoutModification.xml"

	    # get an XMLTextWriter to create the XML
	    $XmlWriter = New-Object System.XMl.XmlTextWriter($filePath,$Null)
	 
	    # choose a pretty formatting:
	    $xmlWriter.Formatting = 'Indented'
	    $xmlWriter.Indentation = 2
	    $XmlWriter.IndentChar = " "

	    #Root - LayoutModificationTemplate
	    $xmlWriter.WriteStartElement('LayoutModificationTemplate')
        $XmlWriter.WriteAttributeString('Version', '1')
	    $XmlWriter.WriteAttributeString('xmlns', "http://schemas.microsoft.com/Start/2014/LayoutModification")

	        #LayoutOptions
	        $xmlWriter.WriteStartElement('LayoutOptions')
            $XmlWriter.WriteAttributeString('StartTileGroupCellWidth', '6')
            $xmlWriter.WriteEndElement()

            #DefaultLayoutOverride
            $xmlWriter.WriteStartElement('DefaultLayoutOverride')

	            #StartLayoutCollection
	            $xmlWriter.WriteStartElement('StartLayoutCollection')

                    #defaultlayout:StartLayout
                    $xmlWriter.WriteStartElement('defaultlayout:StartLayout')
                    $XmlWriter.WriteAttributeString('GroupCellWidth',"6")
                    $XmlWriter.WriteAttributeString('xmlns:defaultlayout',"http://schemas.microsoft.com/Start/2014/FullDefaultLayout")

	                    #start:Group
	                    $xmlWriter.WriteStartElement('start:Group')
	                    $XmlWriter.WriteAttributeString('Name',"Pinned Apps")
                        $XmlWriter.WriteAttributeString('xmlns:start',"http://schemas.microsoft.com/Start/2014/StartLayout")

	                        #start:Tile - Microsoft Edge
	                        $xmlWriter.WriteStartElement('start:Tile')
	                        $XmlWriter.WriteAttributeString('Size',"2x2")
	                        $XmlWriter.WriteAttributeString('Row',"0")
	                        $XmlWriter.WriteAttributeString('Column',"0")
                            $XmlWriter.WriteAttributeString('AppUserModelID',"Microsoft.MicrosoftEdge_8wekyb3d8bbwe!MicrosoftEdge")
	                        #END - start:Tile - Microsoft Edge
	                        $xmlWriter.WriteEndElement()

	                    #END - start:Group
	                    $xmlWriter.WriteEndElement()
                    #END - #defaultlayout:StartLayout
                    $xmlWriter.WriteEndElement()
	            #END - StartLayoutCollection
	            $xmlWriter.WriteEndElement()
	        #END - DefaultLayoutOverride
	        $xmlWriter.WriteEndElement() 
	    #END Root - LayoutModificationTemplate
	    $xmlWriter.WriteEndElement() 

	    $xmlWriter.Flush()
	    $xmlWriter.Close()

	    #Copy to Package 
	    Copy-Item -Path $filePath -Destination $target -Force
    #END of Try
    } Catch {
		Write-Warning "[CATCH] Errors found during Empty-StartMenu, check your inputs. Script Halted."
        Write-Warning $_.Exception.Message
    	BREAK
	}
	
}

##########
# Procedure
##########
Test-environment -ImagePath $Cache -WimFile $Wim
if(!($NoOpen)){Open-Wim -WimPath $Wim -MountPath $Cache}
Clean-AppxPackage -ImagePath $Cache
Empty-StartMenu -ImagePath $Cache
if(!($NoClose)){Close-Wim -MountPath $Cache}