﻿<#
.SYNOPSIS
    Deletes locally saved user profiles.
.DESCRIPTION
    Removes unloaded profiles visible via WMI query.
.PARAMETER Name
    Exact username of the profile that you want to delete.
.EXAMPLE
    .\Nuke-Profiles.ps1
    Deletes all profiles that are not currently loaded on computer.
.EXAMPLE
    .\Nuke-Profiles.ps1 -name "testUser"
    Deletes the profile for username "testUser".
.INPUTS
    No Pipes accepted.
.OUTPUTS
    No outputs sent to the pipe.
.NOTES
    Version: 1705
    Created By: Devin Hunter
    Tested on Windows 7. Admin rights required in order to work.

.LINK
    https://bitbucket.org/Doltknuckle/powershell-toolkit
#>
PARAM(
	[string]$Name #UserName to remove
	)
cls

#Global Parameters
$deleteCount = 0 #Number of users deleted
$logPath = $env:TEMP + "\NukeProfiles.log"

#Log results if run using a script.
if ($myInvocation.InvocationName -like "*.ps1"){
	Write-Host "Logging to: $logPath
"
	Start-Transcript -Path $logPath | Out-Null
	}

#Build User List
$userList = Get-WmiObject -Query "Select LocalPath,SID From Win32_UserProfile Where Special = False AND Loaded = False" #Get a list of accounts to delete
	#Special Accounts and Logged in accounts are filtered out and not deleted.

#Process Userlist
if($userList){ #Check for empty userlist
	if($userlist.Count){$userCount = $userList.Count}else{$userCount = 1}
	Write-Host "Users on system: $userCount
"
	#Process Users
	foreach ($User in $UserList){
		$userSID = $User.SID #Security Identifyer
		$userPath = $User.LocalPath #Filepath to user profile
		
		if(!($name) -or ($userPath.IndexOf($name) -gt 0)){
			Write-Host "userPath, $userSID,"
			$actionFlag = "DELETE"
			} else {
			$actionFlag = "SKIP"
			}
		switch($actionFlag){
			"DELETE"{
				#Delete Profile
				try{
					(gwmi -class Win32_UserProfile -filter "SID='$UserSID'").Delete() #Delete Profile
					$deleteCount++
					}
				catch{
					$errorMsg = $_.Exception.Message
					Write-Host "FAULT - Unable to delete user: $errorMsg"
					}
				#Remove leftover files
				if(Test-Path $User.LocalPath){
					Write-Host "Folder Remaining," #Cleanup user folder if it still exists after SID delete.
					Remove-Item -Recurse -Force -Path $User.LocalPath 
					}
				Write-Host "
----
"
				}
			default {
				#Do Nothing
				}
			}
		}
	Write-Host "Profiles removed: $deleteCount"
	} else {
	Write-Host "No users found."
	}
#Close Transcript
if ($myInvocation.InvocationName -like "*.ps1")	{Stop-Transcript | Out-Null}