Powershell Toolkit
==================
Windows management scripts created for Windows administration.

Scripts are provided AS IS with no guarantee it is functional or continually maintained. If you don't understand how it works, please don't run it.

Contributors
------------
* Devin Hunter (Creator) - devin.hunter@gmail.com

Script Summary
--------------
### Action Scripts
* Add-Font.ps1 : Mass install of fonts on a local system.
* Inject-Package.ps1 : Add package to Win 10 Wim file
* Prep-Wim.ps1 : Take Gold Master version of Win 10 install.wim and clean extra data.
* Nuke-Profiles.ps1 : Delete local profiles from system
### Discovery Scripts
* CheckFor-RemoteFile.ps1 : Generate CSV of files found on a server.
* List-Hardware.ps1: List PNP Hardware installed on local system.

Revision History (vYYMM)
-----------------------------
v1705 - May 2017

* ADD: Add-Font.ps1
* ADD: CheckFor-RemoteFile.ps1
* ADD: Inject-Package.ps1
* ADD: Prep-Wim.ps1
* ADD: Nuke-Profiles.ps1