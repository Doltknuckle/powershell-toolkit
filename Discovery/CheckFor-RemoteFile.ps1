﻿<#
.SYNOPSIS
    Checks remote systems for the existance of a file.
.DESCRIPTION
    This script pulls computer list from active directoy, contacts the computer, and 
    checks for the existance of a file. If no parameters set, will use default 
    vaules listed in Param section.
.PARAMETER ADPath
    Distinguished Name of the OU that you wish to search for computers in.
.PARAMETER ADFilter
    Get-ADComputer filter string. You must have Active Directory installed on your 
    computer and import the Active Directory module for this to be used.
    Use "Import-Module ActiveDirectory" to add to instance.
.PARAMETER Target
    Files that you are searching for, seperated by commas. You must have rights to 
    the hidden admin share for the local hard drive. (Example: \\Server\c$)
.PARAMETER Report
    The file to store the results in CSV format.
.EXAMPLE
    .\CheckFor-RemoteFile.ps1 -Report C:\Folder\ResultList.csv
    This changes the reporting location from the default path to something of your choise.
.EXAMPLE
    .\CheckFor-RemoteFile.ps1 -ADFilter 'ObjectClass -eq "Computer" -and operatingSystem -like "*2016*"' -Target "C:\Folder\FirstFile.txt,C:\Folder\SecondFile.txt"
    Query Server 2016 systems and look for files in specific locations.
.INPUTS
    No Pipes accepted.
.OUTPUTS
    No outputs sent to the pipe.
.NOTES
    Version: 1705
    Created By: Devin Hunter

    Common Filters:
    'ObjectClass -eq "Computer"'
    'ObjectClass -eq "Computer" -and operatingSystem -like "*2008*"'

    Common ADPaths
    "OU=Servers,DC=Contoso,DC=com"

.LINK
    https://bitbucket.org/Doltknuckle/powershell-toolkit
#>
PARAM(
    [string]$ADPath = "OU=Servers,DC=Contoso,DC=com",
    [string]$ADFilter = 'ObjectClass -eq "Computer"',
    [string]$Target = "C:\WF\Test1.txt, C:\WF\Test2.txt",
    [string]$Report = $env:TEMP+"\Result.csv"
)
cls
##########
#Prestart logic
##########

#Script Containers
$binComputerList = @()
$binReport = @()

#Check for AD Module
if(!(Get-Module ActiveDirectory)){
    Write-Host "Active Directory Not Loaded, getting all computers in target OU via system object"
    #Use System Object
    $strFilter = "(&(objectCategory=Computer))" #Target computer objects
    $strLDAP = "LDAP://"+$ADPath #Set LDAP path
    $objDomain = New-Object System.DirectoryServices.DirectoryEntry($strLDAP)
    $objSearcher = New-Object System.DirectoryServices.DirectorySearcher
    $objSearcher.SearchRoot = $objDomain
    $objSearcher.PageSize = 1000
    $objSearcher.Filter = $strFilter
    $objSearcher.SearchScope = "Subtree"
    $objSearcher.PropertiesToLoad.Add("name") | Out-Null
    $objSearcher.FindAll() | Sort-Object Path | ForEach{$binComputerList += $_.Properties.name} #Write names to file 
}

#Load computers using Get-ADComputer
If($binComputerList.count -eq 0){
    Write-Host "Active Directory Module Loaded, using filter to get computers"
    Get-ADComputer -SearchBase $ADPath -Filter $ADFilter | Sort-Object Name | ForEach{$binComputerList += $_.Name}
    Write-Host "Number of computers found:"$binComputerList.count
}

################################################################################
#Primary Data Loop
################################################################################
ForEach($computer in $binComputerList){
    Write-Host "----"
    Write-Host $computer

    #Create/Reset Logging Object
    #Each column is it's own noteproperty. **Remember To: update the export select-object when you add a column**
    $psObject = New-Object psobject #Logging object
    Add-Member -InputObject $psObject -MemberType noteproperty -Name "Name" -Value $computer
    Add-Member -InputObject $psObject -MemberType noteproperty -Name "Result" -Value ""
    Add-Member -InputObject $psObject -MemberType noteproperty -Name "FileFound" -Value ""

    #Ping System
    $pingtest = Get-WmiObject Win32_PingStatus -Filter "Address= `'$computer`'" -ComputerName . -ErrorAction SilentlyContinue
    if(!([bool]$pingtest.ReplySize)){
        Write-Host "No Response to Ping"
        Add-Member -InputObject $psObject -MemberType noteproperty -Name "Result" -Value "No Response to Ping" -Force #Add Logging result
    } else {
        Try{
            $TestResult = $false
            $Target.Split(",") | Foreach{
                $binPath = "\\"+$computer+"\"+$_.Replace(":","$")
                $binResult = Test-Path $binPath -ErrorAction Stop
                if($binResult){
                    $TestResult = $true
                    Add-Member -InputObject $psObject -MemberType noteproperty -Name "FileFound" -Value $_ -Force #Add Logging result
                    }
                }
            if($TestResult){
                Write-Host "File Found"
                Add-Member -InputObject $psObject -MemberType noteproperty -Name "Result" -Value "Found" -Force #Add Logging result
            } else {
                Write-Host "File Missing"
                Add-Member -InputObject $psObject -MemberType noteproperty -Name "Result" -Value "Missing" -Force #Add Logging result
            }
        }Catch{
            $_.Exception.Message
            Add-Member -InputObject $psObject -MemberType noteproperty -Name "Result" -Value $_.Exception.Message -Force #Add Logging result
        }
    }
    $binReport += $psObject #Load reporting object into report array
}

#Make sure that reporting folder exists
$parentFolder = $Report.Substring(0,$Report.LastIndexOf("\"))
if(!(Test-Path $parentFolder)){
    #Report folder doesn't exist
    New-Item -Path $parentFolder -ItemType directory | Out-Null
}

#Export Results
$binReport | Select -property "Name","Result","FileFound" | Export-Csv -Path $Report -NoTypeInformation
