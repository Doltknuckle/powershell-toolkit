﻿<#
.SYNOPSIS
    Discover PNP hardware
.DESCRIPTION
    ################################################################################
    Reports the installed hardware devices on a system. Used to identify non-default
    drivers that are in use on a system. Usefull for SCCM Application Deployment of
    drivers.
.EXAMPLE
    .\List-Hardware.ps1
    Shows all hardware in a preformatted list
.INPUTS
    No Pipes accepted.
.OUTPUTS
    No outputs sent to the pipe.
.NOTES
    Version: 1705
    Created By: Devin Hunter

.LINK
    https://bitbucket.org/Doltknuckle/powershell-toolkit
#>
cls  
#Setup Format
$formatList = @{Expression={$_.DeviceClass};Label="Type";width=10},
    @{Expression={$_.DeviceName};Label="Device Name";width=55},
    @{Expression={$_.DriverProviderName};Label="Provider Name";width=35},
    @{Expression={$_.DriverVersion};Label="Version";width=20},
    @{Expression={$_.DeviceID};Label="Device ID";width=40}

#Run Process
Get-WmiObject Win32_ComputerSystem
Get-WmiObject Win32_PnPSignedDriver | Sort-Object DeviceClass,DeviceName | Format-Table $formatList | out-string -width 160
